
<%@page import="java.text.DecimalFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <title>RESULTADO</title>
    </head>
    <body>
        <p> <h2 class="text-center">RESULTADO DE INTERES SIMPLE</h2></p>
    <br>
    <div class="container h-100">
        <div class="row h-100 justify-content-center align-items-center">
            <br>
            <%
                double monto = (double) request.getAttribute("monto");
                double tasa = (double) request.getAttribute("tasa");
                double tiempo = (double) request.getAttribute("tiempo");
                double interes = (double) request.getAttribute("interes");
                DecimalFormat formDecimal = new DecimalFormat ("###,###.##");
            %>
            <h4> Estimado Cliente: 
                <br> <br>
                &nbsp;Se informa que con un monto capital inicial de &nbsp; &nbsp;<b>$<%=formDecimal.format(monto)%> </b>&nbsp;&nbsp; y con una tasa de &nbsp;&nbsp<b><%=formDecimal.format(tasa)%>%</b>   <br>
                el interes simple obtenido en &nbsp;&nbsp<b><%=formDecimal.format(tiempo)%>  años</b> &nbsp;&nbsp es de    &nbsp;&nbsp<b> $<%=formDecimal.format(interes)%></b> </h4>  <br />
        </div>
        <br> <br>
        <div class="text-center">
            <button onclick="location.href = 'index.jsp'" class="btn btn-primary btn-sm">Realizar un nuevo calculo</button>
        </div>
    </div>        
</html>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
       
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <title>CALCULADORA </title>
    </head>
    <body>
        <h1 class="text-center">CALCULADORA DE INTERES</h1>
        <br>
        <div class="container h-100">
            <div class="row h-100 justify-content-center align-items-center">
                <br>
                <form  name="form" action="controller" method="POST">
                    <div class="form-group">
                        <label for="InputMonto">Monto</label>
                        <input type="number" name="monto" class="form-control" required id="montoInversion" aria-describedby="montoHelp">
                        <small id="montoHelp" class="form-text text-muted">Ingrese aqui el capital inicial.</small>
                    </div>
                    <br>
                    <div class="form-group">
                        <label for="InputTasa">Tasa</label>
                        <input type="number" step="any" name="tasa" class="form-control" required id="tasac" aria-describedby="tasaHelp">
                        <small id="tasaHelp" class="form-text text-muted">Ingrese aqui la tasa de interes.</small>
                    </div>       
                    <br>
                    <div class="form-group">
                        <label for="InputTiempo">Años</label>
                        <input type="number" name="tiempo" class="form-control" required id="tiempoc" aria-describedby="tiempoHelp">
                        <small id="tiempoHelp" class="form-text text-muted">Ingrese aqui el tiempo.</small>
                    </div>        
                    <br>
                    <button type="submit" class="btn btn-success">CALCULAR</button>
                </form>
            </div>
        </div>          
    </body>
</html>
